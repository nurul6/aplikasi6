import { View,ScrollView, Text, Image } from "react-native";
import React from "react";

export default function Cerpen5(){
    return(
        <View>
            <ScrollView>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                <Text style={{fontSize: 20, marginLeft: 10, fontWeight:'bold'}}> Tema   : Ibu </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,marginTop:5}}>       Pagi ini Risa berangkat ke sekolah dengan semangat. Sebelum berangkat tidak 
lupa iya pamit pada Ayahnya yang sedang membaca Koran di depan teras, “Yah  Risa pergi sekolah dulu ya. Hari ini hari terakhir di sekolah sebelum kelulusan 
minggu depan."</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Ayah membalas pamitan Risa dengan senyuman, dan menjawab, “Ya sudah 
hati-hati ya Nak. Jangan pulang terlalu lama, hari ini ada tamu mau bertemu  dengan mu." Risa penasaran siapa tamu yang dimaksud Ayah, “Siapa yang mau 
datang Yah?" Ayah tidak menjawab dan malah menyuruh Risa untuk segera berangkat sekolah dan mengingatkan kembali agar jangan pulang terlalu lama.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Selama di sekolah Risa penasaran siapa tamu yang Ayah maksud. Itulah 
sebabnya setelah semua urusan di sekolah selesai, Risa segera menuju kerumah dengan hati bertanya-tanya siapakah tamu yang ingin menemuiku.
Sesampainya di rumah, Risa langsung disapa oleh seorang wanita. “Halo Risa, perkenalkan nama tante Mia. Tante adalah teman Ayah kamu." Risa perlahan-lahan mencerna siapa dan untuk apa Tante Mia datang ke rumahnya. Apakah 
tante Mia ini tamu yang dimaksud oleh Ayah.Risa kemudian menyapa kembali tante Mia dengan “Halo tante, aku Risa. Ayah 
ada di mana ya tante?" Tante Mia menjawab, “Ayah kamu sedang di belakang  membantu tante menyiapkan makan siang. Kami sudah menunggu Risa sejak 
tadi." Aku kemudian beranjak menuju meja makan dan akhirnya bertemu dengan Ayah. Ayah memeluknya lalu kembali memperkenalkan tante Mia lagi. “Risa, ini 
tante Mia teman Ayah." Risa hanya menganggukan kepala sekali lagi dan kembali memikirkan apa maksud Ayah memperkenalkan tante Mia padanya.
Apakah Ayah ingin menggantikan posisi Ibu dengan Tante Mia? Memikirkan hal tersebut aku seketika menjadi sedih dan tidak bersemangat. Aku tidak mungkin 
mengecewakan Ayah dengan tidak ikut makan siang bersama. Tapi aku merasakan perasaan yang sangat sedih ketika memikirkan apakah benar Ayah 
ingin menggantikan posisi Ibu dengan orang lain. Tidak lama selesai makan siang, tante Mia kemudian pamit pulang. Ayah 
mengantarkannya ke luar dan aku mengunci diriku di kamar. Setelah beberapa saat, Ayah menghampiri pintu kamarku. Ayah mengetuk pintu beberapa kali, 
namun aku tidak ingin berbicara dengan Ayah dahulu. Aku masih belum mengerti kenapa harus ada orang lain yang menggantikan posisi ibu.Tanpa aku sadari Ayah mengajakku berbicara dari balik pintu. Ayah berkata, 
“Risa, Ayah tahu kamu pasti kaget dengan kedatangan tante Mia hari ini. Tapi  Ayah ingin kamu tahu kalau tante Mia itu baik dan Ayah ingin tante Mia 
membantu Ayah menjaga dan membesarkan kamu." Aku menangis mendengar pengakuan Ayah. 
</Text>
                </View>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Aku pun menjawab Ayah sambil 
menangis, “Tapi Risa tidak mau siapapun menggantikan Ibu, yah." Ibu memang 
sudah meninggal sejak 3 tahun yang lalu, tepat saat aku berumur 12 tahun. Saat 
itu aku dan ayah sangat terpukul dengan kematian ibu. Aku tidak pernah 
menyangka bahwa ayah akan secepat ini mencari pengganti ibu. “Tante Mia tidak menggantikan Ibu, Nak. Tante Mia ada untuk membantu 
membesarkan kamu. Banyak hal yang tidak ayah ketahui dalam membesarkan 
kamu menjadi seorang wanita dewasa. Ayah harap kamu bisa mengerti nak." Ucap ayah lagi kali ini. Akupun menyadari ada banyak hal yang harus aku 
mepertimbangkan. Ayah sudah bersusah payah selama tiga tahun terakhir bekerja sekaligus membesarkanku sendirian. Aku harus mengerti ayah dan aku pun membuka pintu kamarku.
“Ayah aku mengerti perasaan Ayah. Jika memang tante Mia adalah pilihan terbaik untuk Ayah, Risa tidak akan menolaknya. Risa tahu Ibu juga bahagia 
ketika Ayah bahagia dan Risa bahagia." Aku memeluk ayah sambil menangis. Aku yakin Ibu mengerti dan tidak akan merasa tergantikan. Ibu tetap anda di hati 
kami. Ibu tetap hidup di hati kami. Aku dan Ayah sayang Ibu.</Text>
                </View>
            </ScrollView>
        </View>
    )
}
