import React , {Component} from 'react';
import { Text, View, StyleSheet,ScrollView, TouchableOpacity, ImageBackground, Image } from 'react-native';

export default class Home extends Component {
    render (){
        return(
            <View>
            <Image source={require('../assets/logo.jpg')} style={{height: 209,width:360, marginLeft:2 }}></Image>
            <View style={{flexDirection:'row', justifyContent:'space-around', marginTop:6}}/>
            <ScrollView horizontal>
            <View style={{flexDirection:'row'}}>
            <View style={{height:92, width:92, backgroundColor:'white' , borderRadius: 10, marginLeft:20, borderColor:'silver', borderBottomWidth: 5}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Sahabat')}>
                    <Image source={require('../assets/shbt.jpg')} style={{height:70, width: 65, marginLeft:10,  marginTop:6}}></Image>
                </TouchableOpacity>
            </View>
            </View>
            <View style={{flexDirection:'row'}}>
            <View style={{height:92, width:92, backgroundColor:'white' , borderRadius: 10, marginLeft:20, borderColor:'silver', borderBottomWidth: 5}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Segumpal')}>
                    <Image source={require('../assets/satu.jpg')} style={{height:60, width: 58, marginLeft:20,  marginTop:10}}></Image>
                </TouchableOpacity>
            </View>
            </View>
            <View style={{flexDirection:'row'}}>
            <View style={{height:92, width:92, backgroundColor:'white' , borderRadius: 10, marginLeft:20, borderColor:'silver', borderBottomWidth: 5}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Cerpen1')}>
                    <Image source={require('../assets/maaf.jpg')} style={{height:80, width: 60, marginLeft:17,  marginTop:6}}></Image>
                </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row'}}>
            <View style={{height:92, width:92, backgroundColor:'white' , borderRadius: 10, marginLeft:20, borderColor:'silver', borderBottomWidth: 5}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Cerpen3')}>
                    <Image source={require('../assets/love.png')} style={{height:53, width: 55, marginLeft:23,  marginTop:10}}></Image>
                </TouchableOpacity>
            </View>
            </View>
            </View>
            </ScrollView>
            <View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 18}}>
                    <View style={{height:135, width:135, backgroundColor:'white', borderRadius: 10, marginLeft:8}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Ibu')}>
                    <Image source={require('../assets/ibu.jpg')} style={{height:100, width:100, marginLeft:15, marginTop:9}}/>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:15, marginTop: 5}}>Cerita Ibu </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{height:135, width:135, backgroundColor:'white', borderRadius: 10, marginLeft:3}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Cerpen2')}>
                    <Image source={require('../assets/baru.jpg')} style={{height:100, width:100, marginLeft:20, marginTop:9}}/>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:26, marginTop: 5}}>USIA 17 DAN PACAR BARU</Text>
                    </TouchableOpacity>
                    </View>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 10}}>
                    <View style={{height:135, width:135, backgroundColor:'white', borderRadius: 10, marginLeft:5}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Seragam')}>
                    <Image source={require('../assets/seragam.jpg')} style={{height:100, width:100, marginLeft:20, marginTop:9}}/>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:20, marginTop: 5}}>Seragam</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{height:135, width:135, backgroundColor:'white', borderRadius: 10, marginLeft:5}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Tentang')}>
                    <Image source={require('../assets/atm.jpg')} style={{height:100, width:100, marginLeft:19, marginTop:9}}/>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:26, marginTop: 5}}>Kartu ATM Ku</Text>
                    </TouchableOpacity>
                    </View>
            </View>

        </View>
       
        );
    };
}






