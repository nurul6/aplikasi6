import {Text, StyleSheet, View, TextInput} from 'react-native';
import React from 'react';

const Inputnama = ({label, placeholder, onChangeText, value, namaState}) => {
    return(
        <>
        <Text style={styles.label}>{label}</Text>
        <TextInput placeholder={placeholder}
        style={styles.area}
        onChangeText={text => onChangeText (namaState,text)}
        value={value}>
        </TextInput>
        </>
    );
};
export default Inputnama;

const styles=StyleSheet.create({
    label :{
        fontSize : 15,
        marginBottom: 5
    },
    area:{
        textAlignVertical :'top',
        borderBottomWidth :1,
        borderColor : 'silver',
        padding:10,
        marginBottom:10
    },
});