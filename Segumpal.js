import { View,ScrollView, Text, Image } from "react-native";
import React from "react";

export default function Segumpal(){
    return(
        <View>
            <ScrollView>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                <Text style={{fontSize: 20, marginLeft: 10, fontWeight:'bold'}}> Tema            :  Segumpal Sesal</Text>
                    <Text style={{fontSize: 20, marginLeft: 10, fontWeight:'bold',}}> Pengarang    : Murti Yuliastuti, Setia Khalfani, Sunu RH, dkk</Text>
                    <Text style={{fontSize: 15, marginLeft: 10, marginTop:5}}>      Mentari senja perlahan kembali ke peraduan. Andini
masih resah menunggu kehadiran Dhani, yang tetap Andini harap akan hadir, sebelum semuanya menjadi hilang begitu saja.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      “Dhani..., apakah memang aku sudah tidak ada
artinya sama sekali buatmu....” Andini masih mencipakkan airnya di pinggir kolam yang terletak di perbatasan kota. Di sinilah Andini bersama Dhani
kerap menghabiskan waktu setelah kuliah.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      Tapi tampaknya hingga matahari benar-benar
tenggelam, sosok pria yang telah sebulan ini sepertinya menghindar bahkan mengacuhkan tanpa ada kejelasan, membuat Andini bertanya-tanya.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      Dari sudut lain, sepasang mata elang Dhani
memandang tajam dan rasanya ingin sekali dia menghampiri Andini untuk bertutur sejujurnya. Tapi
saat dia nekat untuk melepas semua bebannya, sebuah jazz sportif telah menghalangi dia untuk menyambangi Andini.
</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      “Dhin… ayo pulang, sudah mulai petang! Tadi
Papa meminta aku menjemputmu,” Roy mengembangkan senyum pada Andini yang tampak menyimpan rasa kecewa. Sepertinya memang tidak
ada pilihan, taxi juga menjelang Magrib tampak jarang. Dhani sepertinya memang ingin pergi dari
sisinya, itulah yang ada pada pikiran Andini. Untuk pertama kalinya hati Andini tersayat sembilu setelah
kemarin-kemarin Andini mencoba bersabar atas sikap acuh yang ditunjukkan Dhani.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      “Kamu pasti sedang memikirkan Dhani, ya,
Dhin?” tiba-tiba Roy menanyakan sesuatu yang sebenarnya Andini tidak ingin berbagi dengannya. Roy datang seiring menjauhnya Dhani terhadapnya.
Andini sudah berpikir seratus kali untuk menemukan sebuah jawaban “Apakah salahku?”. Dua tahun menjalin sebagai sepasang kekasih tetap tidak bisa
membuat Andini menyelami kesalahan apa yang telah terjadi dalam hubungan mereka. </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      Semua baik-baik saja, Andini merasa sudah
nyaman dengan Dhani, ketua Senat yang bersahaja. Walau Dhani bukan dari keluarga kaya raya, ayahnya hanya seorang guru, tetapi Andini mengagumi Dhani
sebagai pribadi yang matang. Dan sebaliknya Andini merasa Dhani tidak terlalu minder dengan kesenjangan sosial yang tercipta di antara mereka.
Hingga Andini tersadar, secara tiba-tiba Dhani menjauh dan bersikap dingin padanya. </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      “Dhan, kenapa kamu berubah? Apa salahku?
Kalau ada perilaku aku yang melukai hatimu, tolong katakanlah, tapi jangan kau diamkan aku hingga membuat aku bertanya-tanya sendiri tanpa
kejelasan!” Andhini mencoba meraih tangan Dhani saat berkesempatan rehat sejenak dari meeting Senat. </Text>
                </View>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      Tetapi Dhani menghindar dan hanya berkata
dingin, “Dhin, pokoknya aku untuk sementara tidak mau diganggu apa-apa tentangmu. Beri aku waktu untuk melaporkan kinerjaku selama menjabat dari
ketua Senat!” Dan Dhani berlalu meninggalkan gadis yang sebenarnya tidak ingin dilukai seperti ini.</Text>
                    <Text style={{fontSize: 18, marginLeft: 20,}}>Satu bulan lalu… </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>“Oh... jadi di sini, ya, tempat nongkrongnya
ketua Senat kebanggan Universitas Fida Bangsa, merumuskan teori-teori kerja yang tidak ada realisasinya?!” Pak Suryo datang sengaja ke ruangan
Unit Kegiatan Mahasiswa. </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      Dhani hanya diam membisu. Dia tahu sedang
berhadapan dengan siapa. Ucapan barusan semata bukan kalimat yang selayaknya dilontarkan oleh
seorang Pembantu Rektor, yang bukan lain adalah papa kekasihnya, Prastiwi Andini, yang telah dipacari dua tahun.
</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      “Sudah berkali aku peringati, jangan dekati anak
gadisku, karena dia bukan untukmu, Rasdhani Putera! Andini adalah puteri tunggalku dan aku banyak berharap dari dia untuk mendapatkan pasangan yang
sejajar! Bukan mahasiswa yang masuh sibuk berteori dengan visi dan misi yang tidak jelas arahnya!”</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      “Saya bukan orang seperti itu. Saya tahu apa
yang menjadi visi dan misi dalam menjadi seorang leader!” bela Dhani.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      “Alaaah... mahasiswa hanya berkedok mahasiswa
dalam perlindungan universitas! Mana itu reformasi? Mana hasil dari demonstrasi kalian…? Hanya politik praktis! Tidak lebih dari kepentingan ego kalian! Tapi
sudahlah! Aku tidak mau tahu dengan urusan politikmu! Yang jelas, jauhi puteriku! Atau…”</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      “Atau apa? Bapak mengancam saya?” Dhani
mencoba uji nyali dengan pria yang seharusnya dihormati, tapi apalah penghormatan bila harga diri sebagai lelaki diinjak-injak oleh seseorang yang
harusnya lebih mengerti 'kepribadian' atau tata krama'. Hangus itu tata krama dan kepribadian bila kasta mulai dipermasalahkan.
</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>      Jelas papa Andhini tidak merelakan anak
gadisnya berpasangan mahasiswa gembel, yang mengandalkan SPP dengan berbagai beasiswa. Otak cerdasnya pun tidak bisa menjamin dirinya akan
menjadi seorang kaya raya cepat selepas wisuda. Apa yang bisa diharapkan dari bangsa yang hanya hidup untuk orang-orang yang mengandalkan kekayaan,
kolusi, korupsi dan nepotisme. Bapaknya? Tidak mungkin bapaknya yang hanya guru Ibtidaiyah punya
channel untuk dirinya dijamin mendapat pekerjaan yang menjanjikan.</Text>
                </View>
            </ScrollView>
        </View>
    )
}
