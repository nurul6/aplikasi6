import React from 'react';
import {View, Text} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicions from "react-native-vector-icons/Ionicons";


const Tab = createBottomTabNavigator();

import Home from '../pages/Home';
import Setting from '../pages/Setting';
import Menu from '../pages/Menu';



export default function Tabnavigation() {
    return (
        
        <Tab.Navigator >
            <Tab.Screen name="Beranda" component={Home} options={{
        tabBarIcon: () => {
            return <Ionicions name="planet" style={{color: 'black', fontSize:25}}/>;
        },
        }}/>
        <Tab.Screen name="Menu" component={Menu}
        options={{
        tabBarIcon: () => {
        return <Ionicions name="menu" style={{color: 'black', fontSize:25}}/>;
        },
        }}/>
        <Tab.Screen name="Profile" component={Setting}
        options={{
            tabBarIcon: () => {
                return <Ionicions name="person" style={{color:'black', fontSize:25}}/>;
            },
        }}
        />
        </Tab.Navigator>
    );
}
