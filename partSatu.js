import React, {Component} from "react";
import { SafeAreaView, StyleSheet, TextInput, Button, Text } from "react-native";


class PartSatu extends Component {
  state = {
    namaAwal:"",
    namaAkhir:""
  }
  namaAwalTextChange = (inputText) =>{
    this.setState({namaAwal : inputText})
  }

  namaAkhirTextChange = (inputText) =>{
    this.setState({namaAkhir : inputText})
  }

  hasil = () => {
    alert ("halo" + this.state.namaAwal + this.state.namaAkhir)
  }
  render() {
    return (
      <SafeAreaView>
        <TextInput
          style={styles.input}
          onChangeText={this.namaAwalTextChange}
          placeholder = "masukkan nama awal"
        />
        <TextInput
          style={styles.input}
          onChangeText={this.namaAkhirTextChange}
          placeholder = "masukkan nama akhir"
        />
        <Button
          title="tekan saya"
          onPress={this.hasil}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,  
  },
});

export default PartSatu;
