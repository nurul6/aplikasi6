import { View,ScrollView, Text, Image } from "react-native";
import React from "react";

export default function Sahabat(){
    return(
        <View>
            <ScrollView>
            <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
            <Text style={{fontSize: 19, marginLeft: 10, fontWeight:'bold'}}>
Tema          :  Persahabatan</Text>
            <Text style={{fontSize: 19, marginLeft: 10, fontWeight:'bold'}}> Pengarang : Zainal Ariffin Musthofa</Text>
            <Text style={{fontSize: 15, marginLeft: 10, marginTop: 3}}>    Senja yang dulu indah kini menjadi temaram dan bulan yang dulu purnama kini perlahan berubah menjadi sabit.Seperti keadaan hati seorang gadis remaja yang meratapi kekosongan dan kehampaan hatinya karena ditinggal oleh sahabat yang selama ini setia menemaninya baik syka maupun duka.  Dulu, waktu usiaku beranjak 17 tahun, aku
mempunyai beberapa sahabat salah satunya Icha. Icha tinggal di Ciracas, JakartaTimur. Dia anak pertama dari 2 bersaudara, dia adalah seorang remaja yang lugu dan sangat ceria.
Kami bersahabat suddah cukup lama, aku kenal Icha waktu kami sama-sama mendaftar di salah satu SMP favorit di Jakarta. Setelah awal oerkenalan itu,pertemanan kami berlanjut
karena kami diterima di SMP itu. Kami selalu bersama-sama bagai amplop dan perangko yang tak dapat terpisahkan, itulah kami. Kami juga selalu satu kelas. </Text>
       <Text style={{fontSize: 15, marginLeft: 10,}}> Aku mencari tau siapa sebenarnya cowok itu, dari beberapa orang yang aku tanya mereka
mengatakan dia adalah ketua osis, namanya radit, Cuma itu informasi aku dapatkan tentang dia, tapi udah cukup kok. Singkat cerita aku dan kak Radit mnjedi tambah akrab tapi cuma
sebatas teman. Yang tak pernah aku duga ternyata kak Radit naksir sama Icha, aku sedih banget karena dia adalah cinta pertamaku, tapi apa daya aku tak bisa berbuat apa-apa, dan
aku juga sempat kecewa pada Icha karena dia menerima kak Radit menjadi kekasihnya, Icha kan tau kalau aku suka sama kak Radit tapi kenapa dia tega padaku. Mungkin inilah
nasibku, setelah kejadian itu persahabatan aku dan Icha menjadi renggang, aku jarang menyapanya dan sepertinya juga dia sekarang jarang ada waktu buat kita berdua sama-sama lagi seperti dulu. Lagi pula aku tak sekelas dengannya
Waktu terus berputar, tanpa terasa tahunpun berganti. Akhir-akhir ini aku melihat Icha
tampak murung dan gak seperti biasanya yang sangat ceria. Walau aku belum bisa
memaafkan Icha tapi walau bagaimanapun dia adalah sahabatku dan aku harus tau apa
yang sedang terjadi. Satauku dari berita yang beredar kalau Icha mengidap penyakit tumor
yang bersarang diperutnya sejak beberapa tahun ini, sejak dokter memfonis penyakit itu
Icha berubah menjadi nak yang pemurung danpendiam. Aku sangat merasakan perubahan
itu, tapi setiap kali aku tanya dia tak pernah mau cerita dan jujur padaku. Menurutku dia
berubah menjadi seperti itu karena mungkin dia merasa hidupnya tak akan lama lagi.
<Text style={{fontSize: 15, marginLeft: 10,}}>Seiring berjalannya waktu perut Icha makin membesar, aku belum percaya dengan apa
yang temen-temen bilang padaku. Aku desak Icha untuk menceritakan apa yang terjadi
padanya, akhirnya Icha mau bercerita. Aku sempat terkejut mendangarnya sekaligus sedih
bercampur dengan rasa kekecewaan, mengapa baru seekarang dia cerita semua itu padaku. Tapi mungkin karena aku tak sedekat dulu sama dia. Aku juga denger-denger dari yang laen Icha putus, 
Icha diputuskan kak Radit karena keadaan Icha dg perut yang makin
membesar.</Text>
 </Text>
        </View>
        <View >
            
<View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
    <Text style={{fontSize: 15, marginLeft: 10,}}>   Icha masih tetap sekolah, tapi lama kelamaan dia merasa kecil hati dan malu. Dengan
kondisi tubuh yang semakin menurun, sampai akhirnya Icha dirawat di Rumah sakit Haji
Pondok Gede. Aku dan teman-taman menjenguknya untuk memberikan semangat dan
dukungan padanya agar Icha gak semakin drop dan putus asa. Hanya sampai disitu saja
kabar yang aku dengar tentang Icha, disatu sisi aku masih kecewa padanya tapi disisi lain
aku juga mempersiapkan UN.  Pagi hari yang sangat gelap karena hujan turun begitu derasnya, aku sedang duduk
melamun memikirkan bagaimana keadaan Icha sekarang, tiba-tiba aku dikejutkan dengan
ringtone handphoneku yang berbunyi dank u lihat dilayar hpku ternyata mamanya Icha
memanggil, fikirku tumben tapi ada apa ya, kok pagi-pagi gini tante telfon aku. “halo
assalamu'alaikum, bisa bicara dengan Cika?”, nada suara mama Icha tampak berat, sepertinya dia sedang menangis. “ii…aaa tante, ada apa kokpagi-pagi begini telfon Cika?
Trus bagaimana kabar Icha tante?” tanyaku agak ragu, “Icha telah berpulang Ka” belum
sempat aku mengucapkan turut berduka cita pada tante, tut…tut…tut…tut telfon tiba-tiba
terputus.  Aku menangis dan menyesali dengan semua yang terjadi, dihatiku tersirat
penyesalan yang amat mendalam, aku terlalu jahat dan egois pada Icha dan gak pernah
meluangkan waktu untuk menjenguk sahabatku sendiri yang menjalani hari-hari akhirnya
sendirian, tanpa aku. “Maafkan sahabatmu ini Ca…..hik..hik..hik…!!!” tangisku
Aku datang ke rumah Icha untuk melihat dia terakhir kalinya dan mengucapkan bela
sungkawa pada keluarga Icha. Setibaku disana aku melihat Icha terbaring kaku, dikelilingi
orang-orang yang membaca yasin untuknya, tiba-tiba pandanganku menjadi gelap. “Icha…..” panggilku, “sudahlah Ka, relakanlah kepergian Icha, agar dia tenang di Alam
sana” mama Icha ada disampingku, dan memberikan selembar kertas padaku, “ini dari Icha
buat kamu, dia menulis pada saat kamu jarang menemuinya, tante tinggal dulu kebawah”. “makasih tante dan Cika minta maaf kalo selama ini Cika gak pernah menjenguk dia, Cika
lagi UN tante,” aku menangis. “gak apa-apa kok tante ngerti, kamu ada masalah ya sama
Icha?” tanya mama Icha, “eng…enggak kok tante, kami berdua baik-baik saja””ya udah
jangan nangis lagi, tante ke bawah bdulu ya” tante pun meninggalkanku sendiri di kamar
Icha karena Perlahan-lahan tadi aku pingsan, aku melihat foto-foto yang ada dimeja samping tempat tidur, betapa lembutnya senyum Icha di foto itu. aku buka kertas
ituperlahan-lahan, dan aku pun mulai membaca kata demi kata disurat itu. Sebelumnya gue minta maaf atas kejadian kemaren”, bukan maksud gue untuk merebut kak
Radit dari lo, tapi gue juga cinta dia dan gue juga udah putus ma dia, karena dia bukan laki-laki yang baik. O ya, lo tau kan kalo gue gak bisa buat puisi kayak lo, tapi ini puisi gue buat
khusus sahabat sejati gue ini, maaf ya kalo buatan gue gak sebagus puisi-puisi lo,
heheheh……..</Text>
</View>
<View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
<Text style={{fontSize: 34, marginLeft: 10,textAlign:'center', fontWeight: 'bold'}}>Surat Terakhir</Text>
<Text style={{fontSize: 15, marginLeft: 10, textAlign:'center'}}> Butir-butiran air mata yang jatuh setetes demi setetes
Menemani dan menjadi saksi saat ku tulis suratku yang terakhir
Jika hanya derita yang harus aku terima Jika hanya kemitian yang harus ku alami
Aku bersedia menjalani tanpa kesedihan Namun ketika kau berucap bahwa untukku
Sudah tak ada lagi maaf terasa lemah lunglai tubuh ini Sahabat yang slalu mengisi hari-hariku
Seberapa besarpun salah yang ku pandang Seberapa rendah budi yang ku jalani…maafkan aku
Derita karena bersalah berlarut-larut tanpa henti Dan tampaknya Tuhan sudah berkenan menjemputku
Jangan menangis sahabat….walau tak terkatakan Sungguh aku merasa kau telah memaafkanku
Slamat tinggal sahabat sejatiku Ikhlaskanlah kepergiankui
Semoga sepeninggalku dari sisimu Bahagian akan slalu menemanimu Miss u sobat</Text>
<Text style={{fontSize: 22, marginLeft: 25,textAlign:'center', fontWeight: 'bold'}}>ICHA</Text>

<Text style={{fontSize: 15, marginLeft: 10,marginTop: 22}}> Keesokan harinya Aku baru sadar ternyata Icha hari ini berulang tahun yang ke 17, aku
bermalam di rumah Icha, dan pagi-pagi aku segera kebawah dan akan mengikuti pemakaman Icha. Sebenarrnya aku tak sanggup melihat makam itu, karena akan
mengingatkanku akan kenangan” kami berdua dulu, tapi aku coba untuk tegar untuk melangkahkan kaki menuju makamnya. 
Setelah pemakaman selesai dan semua orang pulang, aku sendiri di makam itu, sepi. 
Aku menangis disamping nisan Icha, walau tersendat-sendat dan terbata karena aku nangis aku nyanyikan lagu happy birthday buat
Icha, dan memandangi nisan yang ada dihadapanku saat ini, makam yang sunyi, aku masih menangis sendiri di makam bisu itu,
sebelum pulang aku meninggalkan secarik kertas balasan surat Icha, walau mungkin tak akan pernah dibaca olehnya, 
tapi itulah kenanganterakhirku buat Icha. Kenangan indah tentang kita akan slalu ku ingat setiap detiknya
Jika ku tutup mataku, aku masih dapat melihatmu Kau memperlihatkan senyum termanismu
Tapi itu hanya lamunan sesaatku Kini kau telah jauh tinggalkanku
Aku belum sempat meminta maaf padamu dan menyayangimu Dan tak ingin kau pergi jauh
Tinggalkan kenangan kita bersama Tapi takdir berkatab lain Terlalu cepat Tuhan memanggilmu
Hanya sebuah puisi ini aku persembahkan untukmu Kepergianmu, meninggalkan kisah yang sangat pahit bagiku
Aku akan selalu mengenangmu, sahabat terbaikku Semoga kau tenang disana Suatu saat kita pasti akan bertemu kembali
</Text>
<Text style={{fontSize: 15, marginLeft: 10, marginTop:6}}> " TAMAT......." </Text>
</View>
        </View>
        </ScrollView>
        </View>

    );
}