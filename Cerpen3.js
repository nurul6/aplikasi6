import { View,ScrollView, Text, Image } from "react-native";
import React from "react";

export default function Cerpen3(){
    return(
        <View>
            <ScrollView>
            <View style={{flexDirection:'row'}}>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                    <Text style={{fontSize: 20, marginLeft: 10, fontWeight:'bold'}}>Tema        : Love Story (Cerita Cinta)</Text>
                    <Text style={{fontSize: 20, marginLeft: 6, fontWeight:'bold'}}>Pengarang :  I Gede Rudiana</Text>
                    <Text style={{fontSize: 15, marginLeft: 4, marginTop:8}}>      Kisah ini berawal dari pertemuan seorang wanita dengan lelaki yang sama-sama sedang 
melaksanakan sebuah kegiatan kemah. Jin itulah nama lelaki sedangkan Sinta nama wanita tersebut. Mereka berbeda sekolah tetapi karena memang kegiatan kemah bakti tersebut 
dilaksakan secara serempak oleh semua SMA maka mereka akhirnya sama-sama bertemu di perkemahan tersebut.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,marginTop:5}}>Letak tempat kemah Jin dan Sinta tersebut sebenarnya cukup jauh, tetapi karena mereka sering 
apel pagi bersama seluruh siswa, akhirnya tatapan-tatapan kecilpun tidak terhindari. Perlahan Jin mulai berani mendekati Sinta jika ada waktu senggang istirahat. Tetapi belum berani untuk 
berkenalan, Jin hanya tersenyum kecil jika lewat didepan Sinta. Akhirnya setelah beberapa kali bertemu dan saling melempar senyum, mereka berkenalan.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10, marginTop:5}}>"Hei, hari ini panas banget ya?"
"Iya panas nih, mungkin karena gunung ini udah gundul kali ya, makanya panas." "Anyway, kita belum kenalan kan? Nama aku Jin. Nama kamu siapa?"
"Namaku Sinta"</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Begitulah proses perkenalan mereka yang sangat singkat tersebut akhirnya membuat keduanya 
semakin dekat dari hari kehari. </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Kemah sudah hampir satu minggu mereka laksanakan, dan keduanya juga semakin akrab. 
Sampai akhirnya mereka berdua harus terpisah karena kegiatan kemah telah usai. Tetapi kemah berakhir atau tepatnya di acara Malam Keakraban, Jin sengaja berada disamping Sinta agar jika 
mereka tidak bertemu lagi, maka tidak ada kesedihan karena rasa rindu. </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>"Sin, klo kegiatan ini udah kelar, kita masih bisa ketemu lagi ga ya?" Tanya Jin.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>"Aku juga ga tau Jin, tetapi klo emang kita ga bisa ketemu lagi, gimana?" Jawab Sinta.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>"Yang pasti aku bakal kehilangan kamu banget" Jin terlihat sedih dengan mengatakan hal tersebut.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Begitulah malam keakraban mereka lalui dengan kesedihan karena kemah akan usai besok dan 
mereka akan segera kembali kerumah masing-masing.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Esok hari pun tiba, saat dimana mereka harus segera persiapan untuk pulang. Jin yang masih 
merasakan sedih akhirnya memiliki niat untuk menembak Sinta untuk dijadikannya seorang pacar, agar mereka bisa terus bertemu jika keduanya telah sama-sama sudah kembali kerumah.
Didekatinya Sinta yang saat itu sedang mengepak pakaiannya.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>"Boleh ngomong sebentar ga?" Jin mendekat.</Text>
                </View>    
            </View>
            <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                    <Text style={{fontSize: 15, marginLeft: 10, }}>"Boleh, ngomong aja Jin, ada apa?" Sinta berdiri dan mendekat ke Jin yang ada di luar camp.
</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>"Tapi ga disini, bisa kita kedepan sebentar?" "Oh, yaudah yuk!"</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Mereka berdua kedepan camp dan disitulah Jin mengatakan bahwa dia saya kepada Sinta. 
Akhirnya mereka berdua telah menjadi sepasang kekasih karena Sinta ternyata juga menyukai 
Jin.Dengan bertukar no handphone dan alamat rumah, mereka berharap akan bisa menjalin  hubungan yang langgeng nantinya</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Kegiatan camp sudah berlalu sekitar 2 hari, Sinta dan Jin juga seperti biasa melakukan 
aktifitasnya masing-masing dengan tetap saling menjaga komunikasi mereka berdua. Sejak hari pertama mereka sampai dirumah, Jin sudah main kerumah Sinta. Dia juga 
berkenalan dengan orang tuanya yang terlihat sangat ramah kepada Jin. "Tau ga aku bawa apa?" Tanya Jin "Bawa apaan sih, kok repot-repot?" Sinta seakan melarang Jin membawa sesuatu.
"Merem dulu dong, nanti baru aku kasih surprisenya" "Oke, deeehhh..."</Text>
                    <Text style={{fontSize: 16, marginLeft: 10,}}>"SURPISEEEEE....!!!" </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>"Ya ampun Jin kok sampe segitunya sih" Jawab Sinta yang terlihat bahagia karena dibawakan 
sebuah bonek pink yang lucu.Begitulah kejadian hari pertama yang sangat berkesan untuk keduanya karena sudah 2 hari 
sejak camp mereka tidak bertemu.Sejak hari pertama itulah Jin menawarkan Sinta untuk menjemputnya tiap hari jika berangkat kesekolah. Mereka tersenyum bahagia setiap hari karena bertemu dan bercanda bersama. Rasa sayang dan 
cinta semakin dalam mereka rasakan satu sama lain. Sampai dengan hari ke 56 mereka bersama 
atau tepatnya hampir 2 bulan bersama memadu kasih dan cinta berdua. Tidak lupa Jin juga 
selalu membawa sebuah boneka tiap hari mereka bertemu. Walaupun mereka berdua selalu bersama dan terlihat bahagia, sedikitpun Jin tidak pernah 
mengatakan "Aku Sayang Padamu" kepada Sinta. Sinta-pun merasa semakin lama tidak nyaman karena hal tersebut. Suatu ketika saat Jin main kerumahnya, Sinta berkata kepada Jin.
"Jin, kamu sayang ga sih sama Sinta?" "Ehmm, Aku....aku pulang dulu ya, ini bonekanya buat kamu"
"Lho kok malah pulang" </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Sinta tidak habis pikir, kenapa Jin tidak mau mengatakan sayang kepada dirinya. Saat ditanya 
sayang atau ga, malah Jin bergegas pulang. Sampai terbesit dalam pikiran Sinta, apakah Jin 
tidak serius sayang kepada dia ya? Lalu tiba akhirnya Sinta yang akan merayakan hari ulang tahunnya yang ke 17 tahun, dan besar 
harapan Sinta agar Jin bisa datang ke hari tesebut. Karena ini merupakan hari yang sangat penting bagi dirinya, Sinta berpikir tentu Jin akan memberikan sebuah surprise yang sangat 
indah sepanjang hidupnya. Ulang tahun Sinta jatuh 2 hari lagi, dirinya terlihat selalu bahagia saat bersama dengan Jin.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>"Kamu kenapa sih kok senyum-senyum terus"
"Ah ga pa2 kok, cuma seneng aja"</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>"OK"</Text>
                </View>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                <Text style={{fontSize: 15, marginLeft: 10,}}>Begitulah Jin yang terlihat sangat dingin menjawab kebahagiaan Sinta. Lalu tiba saatnya ulang 
tahun Sinta yang ke 17 yang tepat jatuh pada hari minggu. Dengan mengenakan sebuah gaun pesta yang cantik, Sinta menyambut semua tamu undangan yang pada sore itu datang 
kerumahnya, karena memang acaranya berlangsung pada sore hingga malam. Sudah pukul 8 malam, Jin belum terlihat datang di pesta ulang tahun Sinta. Dia mulai cemas 
dan berpikir sesuatu yang buruk mungkin terjadi kepada Jin. Waktu terus berjalan, dan semua tamupun telah pergi karena acara telah usai. Sinta semakin 
cemas dan tidak karuan karena Jin tidak kunjung tiba. Dia mencoba menelpon ke no handphone 
Jin tetapi tidak aktif. Dengan perasaan campur aduk, Sinta terus menunggu Jin yang belum datang tersebut. Sembari 
berdoa dia juga berpikir, "Apa Jin lupa hari ulang tahunku?" begitulah yang dipikirkan oleh dirinya.</Text>
                <Text style={{fontSize: 15, marginLeft: 10,}}>Pagi pun tiba, Sinta yang saat itu tertidur di sofa depan tidak menemukan Jin datang pada 
                malam hari ulang tahunnya. Sembari menangis karena kecewa akhirnya Sinta segera mandi dan bergegas berangkat kesekolah.</Text>
                <Text style={{fontSize: 15, marginLeft: 10,}}>Hari itu Jin tidak terlihat menjempur Sinta seperti biasanya. Merasa aneh dan seakan masih 
kurang percaya Jin lupa dengan hari ulang tahunnya, dia akhirnya memutuskan untuk mampir 
kesekolah Jin terlebih dahulu sebelum berangkat kesekolahnya.
Sesampainya di sekolah, apa yang Sinta temukan? ternyata Jin sedang bercanda dengan teman-teman wanitanya sembari tertawa seakan mereka akrab satu sama lain. Sinta yang melihat 
pemandangan yang menyakitkan tersebut akhirnya mendekati mereka dan marah kepada Jin. "Oh jadi gini ya kelakuan kamu selama ga sama aku?"
  "Si..Sinta?" </Text>
                <Text style={{fontSize: 15, marginLeft: 10,}}>Dengan marah Sinta segera bergegas pergi meninggalkan mereka. Anehnya Jin tidak mengejar 
Sinta yang marah karena peristiwa tersebut. Sinta yang menangis saat itu seakan tidak 
diperdulikan oleh Jin. Seharian dari sepulangnya Sinta dari sekolah, hanya menangis karena sakit hati kepada Jin. 
Dari pagi sampai malam dia hanya menangis terus menerus dengan masih terpikir peristiwa 
tadi pagi saat Jin bersama dengan wanita lain. Sudah berkali-kali Jin menelpon tetapi Sinta mengabaikan panggilan dari Jin tersebut dan 
berjanji tidak akan pernah mau lagi bertemu dengan Jin sampai kapanpun. Lalu selang beberapa jam, bel sepeda motor berbunyi dari luar rumah Sinta. Sinta yang saat itu 
memang belum tidur mendapat pesan singkat yang berbunyi "Tolong kamu keluar sebentar 
Sinta, aku mau ngomong sama kamu". Sinta yang memang sangat mencintai Jin akhirnya tidak menolak untuk keluar rumah untuk 
menemui Jin yang saat itu berada diluar rumah. "Mau apa lagi kamu?! ga puas kamu udah nyakitin aku hari ini" Tegas Sinta dengan nada tegas 
dan marah. "Aku mau minta maaf sama kamu" Jawab Jin dengan nada lemah.
"Maaf kata kamu?! kamu ga sadar sudah buat aku sakit seperti ini?!"</Text>
                <Text style={{fontSize: 15, marginLeft: 10,}}>"Sinta, aku...mau ngasih ini ke kamu...."</Text>
                <Text style={{fontSize: 15, marginLeft: 10,}}>Jin membawa sebuah boneka besaaaar sekali yang tidak biasanya boneka yang dia berikan 
kepada Sinta. Tetapi Sinta semakin marah besar kepada Jin</Text>
                </View>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                <Text style={{fontSize: 15, marginLeft: 10,}}>"Buat apa lagi kamu bawain boneka lagi ke aku? apa dengan membawa boneka itu, aku akan 
memaafkan kamu Jin? TIDAAAAKKK....!!!" Dengan merebut bonek yang dipegang oleh Jin, dibuangnya kejalan boneka tersebut. Dengan 
perasaan sedih Jin kaget dan perlahan berjalan menuju ke boneka tersebut berniat  mengambilnya. Tiba-tiba terdengar dari jarak jauh sebuah mobil yang melaju kencang mengarah ke Jin yang 
tepat berada ditengah sedang mengambil boneka tersebut. Lalu...."BRAAAKKK...!!!!" Jin tertabrak dan Sinta berteriak, TIDAAAAAKKKK....!!! JIIIINNNN.....!!!</Text>
                <Text style={{fontSize: 15, marginLeft: 10,}}>Mobil tersebut menghempaskan tubuh Jin sampai beberapa kilometer. Jin meninggal dunia 
seketika karena pendarahan pada bagian kepalanya. Sinta seketika itu pingsan dan orang-orang yang berada disekitar segara mengamankan tubuh Jin yang sudah tidak bernyawa lagi.
Jin telah tiada, hanya sebuah kenangan Sinta akan kepedihan yang tidak akan pernah kembali untuk selamanya. Sinta merasa sangat bersalah karena dirinya lah yang menyebabkan kematian 
orang yang paling dia sayangi. Lalu sembari menangis, dia kembali mencoba mengenang semua kenangan indah bersama 
dengan Jin. Dilihatnya sekeliling kamar sambil meneteskan air mata, kamar yang penuh dengan 
boneka-boneka pemberian Jin. Dengan bertambah sedih Sinta memeluk boneka tersebut erat-erat, tiba-tiba "I LOVE YOU..." "I LOVE YOU..."
"hah?" Sinta kaget... </Text>
                <Text style={{fontSize: 15, marginLeft: 10,}}>Dari perut boneka tersebut berbunyi "I LOVE YOU" Lalu Sinta mulai memencet perut boneka 
lainnya lalu, "I LOVE YOU" semakin tidak percaya dipencet lah semua perut boneka pemberian dari Jin yang berjumlah 59 boneka tersebut. Semakin deras air mata Sinta, "kenapa 
aku ga sadar klo selama ini Jin selalu ingin mengatakan cinta kepadaku lewat boneka-boneka yang ia berikan kepadaku?"
Lalu Sinta mengambil boneka yang paling besar dan merupakan boneka yang ke-60 pemberian Jin kepada dirinya sebelum jin meninggal. Dipencetnya pula perut dari boneka tersebut. Lalu 
terdengarlah rekaman suara Jin, dia mengatakan seperti ini" "Sudah 60 hari kita bersama Sinta, tetapi sampai hari ini pun aku belum bisa mengatakan kata 
cinta kepadamu. Tiap hari aku berharap bahwa boneka ini bisa mewakili kata cintaku tersebut 
untuk dirimu, tetapi ternyata kamu tidak pernah menyadarinya. Aku tahu bahwa aku adalah 
seorang pengecut, tetapi yang perlu kamu tahu kalau aku....cinta....kamu sampai akhir hayatku"
Begitulah rekaman suara Jin yang dia berikan sesaat sebelum Jin kecelakaan dan merenggut 
dirinya. Sinta menangis dan tidak pernah akan mendapatkan kembali sosok Jin yang ternyata 
sangat-sangat mencintainya</Text>
                </View>
            </ScrollView>
        </View>
    )
}
