import React, {Component} from 'react';
import { Text, View,Image,Alert, StyleSheet, TouchableOpacity } from 'react-native';
import Inputnama from '../pages/Inputnama';



export default class Setting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nama :'',
            alamat:'',
            email:'',
        };
    }
    onChangeText = (namaState,Value) => {
        this.setState({
            [namaState] : Value
        });
    };
    onSubmit = () => {
        if (this.state.nama && this.alamat && this.email){
            const memberrefensi = FIREBASE.database().ref('member');
            const member = {
                nama : this.state.nama,
                alamat : this.state.alamat,
                email : this.state.email,
            }
            memberrefensi
            .push(member)
            .ThemeContext((data)=> {
                Alert.alert('sukses','Data Tersimpan');
                this.props.navigation.replace('Data');
            })
            .catch((error) => {
                console.log("error : ",error);
            })
        }else {
            Alert.alert('sukses','Data sudah masuk');
        }
    };
    render() {
        return (
            <View style={styles.page} >
                <Inputnama 
                label = "Nama !"
                placeholder = "Masukkan nama"
                onChangeText = {this.onChangeText}
                value = {this.state.nama}
                namaState="Nama"
                />
                <Inputnama
                label = "Alamat !"
                placeholder = "Masukkan alamat anda"
                onChangeText = {this.onChangeText}
                value = {this.state.alamat}
                namaState="Alamat"
                />
                <Inputnama
                label = "Email !"
                placeholder = "Masukkan email anda"
                onChangeText = {this.onChangeText}
                value = {this.state.email}
                namaState="Email"
                />
                <TouchableOpacity style={styles.button} onPress={() => this.onSubmit()}>
                    <Text style={styles.textbutton}>SIMPAN</Text>
                </TouchableOpacity>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    page: {
        flex : 1,
        padding : 30
    },
    button : {
        backgroundColor :'grey',
        padding : 10,
        borderRadius : 5,
        marginTop : 30
    },
    textbutton : {
        color : 'white',
        fontWeight : 'bold',
        textAlign : 'center',
        fontSize : 16
    },
});

