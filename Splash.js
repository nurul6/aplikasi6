import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';


const Splash = ({navigation}) => {
    useEffect (() => {
        setTimeout (() => {
            navigation.replace('Home');
        }, 3000);
    });
    return(
        <View style={styles.wrapper}>
            <Text style={styles.welcomeText}>Welcome To The World Of Short Stories</Text>
            <Image source={require('../assets/Gmbar1.jpg')} style={{width:240, height:240}}/>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    welcomeText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'grey',
        paddingBottom: 15,
    },
});
