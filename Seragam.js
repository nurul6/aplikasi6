import { View,ScrollView, Text, Image } from "react-native";
import React from "react";

export default function Seragam(){
    return(
        <View>
            <ScrollView>
            <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                <Text style={{fontSize: 20, marginLeft: 10, fontWeight:'bold'}}> Tema       : Seragam</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,marginTop:5}}>  Lelaki jangkung berwajah terang yang membukakan pintu terlihat takjub begitu 
mengenali saya. Pastinya dia sama sekali tidak menyangka akan kedatangan saya 
yang tiba-tiba. Ketika kemudian dengan keramahan yang tidak dibuat-buat dipersilakannya saya 
untuk masuk, tanpa ragu-ragu saya memilih langsung menuju amben di seberang  ruangan. Nikmat rasanya duduk di atas balai-balai bambu beralas tikar pandan itu.
Dia pun lalu turut duduk, tapi pandangannya justru diarahkan ke luar jendela, pada pohon-pohon cengkeh yang berderet seperti barisan murid kelas kami dahulu
saat mengikuti upacara bendera tiap Isnin. Saya paham, kejutan ini pastilah  membuat hatinya diliputi keharuan yang tidak bisa diungkapkannya dengan kata-kata. Dia butuh untuk menetralisirnya sebentar.
Dia adalah sahabat masa kecil terbaik saya. Hampir 25 tahun lalu kami berpisah karena keluarga saya harus boyongan ke kota tempat kerja Ayah yang baru di luar
pulau hingga kembali beberapa tahun kemudian untuk menetap di kota kabupaten. Itu saya ceritakan padanya, sekaligus mengucapkan maaf karena sama sekali 
belum pernah menyambanginya sejak itu. ”Jadi, apa yang membawamu kemari?”
”Kenangan.” ”Palsu! Kalau ini hanya soal kenangan, tidak perlu menunggu 10 tahun setelah 
keluargamu kembali dan menetap 30 kilometer saja dari sini.” Saya tersenyum. Hanya sebentar kecanggungan di antara kami sebelum kata-kata 
obrolan meluncur seperti peluru-peluru yang berebutan keluar dari magasin. Bertemu dengannya, mau tidak mau mengingatkan kembali pada pengalaman 
kami dahulu. Pengalaman yang menjadikan dia, walau tidak setiap waktu, selalu lekat di ingatan saya. Tentu dia mengingatnya pula, bahkan saya yakin rasa yang</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>diidapnya lebih besar efeknya. Karena sebagai seorang sahabat, dia jelas jauh 
lebih tulus dan setia daripada saya. Malam itu saya berada di sini, memperhatikannya belajar. Teplok yang menjadi 
penerang ruangan diletakkan di atas meja, hampir mendekat sama sekali dengan wajahnya jika dia menunduk untuk menulis. Di atas amben, ayahnya santai 
merokok. Sesekali menyalakan pemantik jika bara rokok lintingannya soak bertemu potongan besar cengkeh atau kemenyan yang tidak lembut diirisnya. 
Ibunya, seorang perempuan yang banyak tertawa, berada di sudut sembari bekerja memilin sabut-sabut kelapa menjadi tambang. Saat-saat seperti itu ditambah 
percakapan-percakapan apa saja yang mungkin berlaku di antara kami hampir setiap malam saya nikmati. Itu yang membuat perasaan saya semakin dekat 
dengan kesahajaan hidup keluarganya. Selesai belajar, dia menyuruh saya pulang karena hendak pergi mencari jangkrik. 
Saya langsung menyatakan ingin ikut, tapi dia keberatan. Ayah dan ibunya pun melarang. Sering memang saya mendengar anak-anak beramai- ramai berangkat 
ke sawah selepas isya untuk mencari jangkrik. Jangkrik-jangkrik yang diperoleh nantinya dapat dijual atau hanya sebagai koleksi, ditempatkan di sebuah kotak, 
lalu sesekali digelitik dengan lidi atau sehelai ijuk agar berderik lantang. </Text>
                </View>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Dari apa 
yang saya dengar itu, proses mencarinya sangat mengasyikkan. Sayang, Ayah tidak pernah membolehkan saya. Tapi malam itu toh saya nekat dan sahabat saya 
itu akhirnya tidak kuasa menolak.”Tidak ganti baju?” tanya saya heran begitu dia langsung memimpin untuk 
berangkat. Itu hari Jumat. Seragam coklat Pramuka yang dikenakannya sejak pagi masih akan terpakai untuk bersekolah sehari lagi. Saya tahu, dia memang tidak 
memiliki banyak pakaian hingga seragam sekolah biasa dipakai kapan saja. Tapi memakainya untuk pergi ke sawah mencari jangkrik, rasanya sangat-sangat tidak elok.
”Tanggung,” jawabnya. Sambil menggerutu tidak senang, saya mengambil alih obor dari tangannya. Kami
lalu berjalan sepanjang galengan besar di areal persawahan beberapa puluh meter setelah melewati kebun dan kolam gurami di belakang rumahnya. Di kejauhan, 
terlihat beberapa titik cahaya obor milik para pencari jangkrik selain kami. Rasa hati jadi tenang. Musim kemarau, tanah persawahan yang pecah-pecah, gelap 
yang nyata ditambah angin bersiuran di areal terbuka memang memberikan sensasi aneh. Saya merasa tidak akan berani berada di sana sendirian.
Kami turun menyusuri petak-petak sawah hingga jauh ke barat. Hanya dalam beberapa menit, dua ekor jangkrik telah didapat dan dimasukkan ke dalam 
bumbung yang terikat tali rafia di pinggang sahabat saya itu. Saya mengikuti dengan antusias, tapi sendal jepit menyulitkan saya karena tanah kering 
membuatnya berkali-kali terlepas, tersangkut, atau bahkan terjepit masuk di antara retakan-retakannya. Tunggak batang-batang padi yang tersisa pun bisa menelusup 
dan menyakiti telapak kaki. Tapi melihat dia tenang-tenang saja walaupun tak memakai alas kaki, saya tak mengeluh karena gengsi.
Rasanya belum terlalu lama kami berada di sana dan bumbung baru terisi beberapa ekor jangkrik ketika tiba-tiba angin berubah perangai. Lidah api 
bergoyang menjilat wajah saya yang tengah merunduk. Kaget, pantat obor itu justru saya angkat tinggi-tinggi sehingga minyak mendorong sumbunya terlepas. 
Api dengan cepat berpindah membakar punggung saya! ”Berguling! Berguling!” terdengar teriakannya sembari melepaskan seragam 
coklatnya untuk dipakai menyabet punggung saya. Saya menurut dalam kepanikan. Tidak saya rasakan kerasnya tanah persawahan atau tunggak-tunggak 
batang padi yang menusuk-nusuk tubuh dan wajah saat bergulingan. Pikiran saya hanya terfokus pada api dan tak sempat untuk berpikir bahwa saat itu saya akan 
bisa mendapat luka yang lebih banyak karena gerakan itu. Sulit dilukiskan rasa takut yang saya rasakan. Malam yang saya pikir akan menyenangkan justru 
berubah menjadi teror yang mencekam! Ketika akhirnya api padam, saya rasakan pedih yang luar biasa menjalar dari 
punggung hingga ke leher. Baju yang saya kenakan habis sepertiganya, sementara 
sebagian kainnya yang gosong menyatu dengan kulit. Sahabat saya itu tanggap melingkupi tubuh saya dengan seragam coklatnya melihat saya mulai menangis 
dan menggigil antara kesakitan dan kedinginan. Lalu dengan suara bergetar, dia mencoba membuat isyarat dengan mulutnya. 
</Text>
                </View>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                    <Text style={{fontSize: 15, marginLeft: 10,}}> Sayang, tidak ada seorang pun yang 
mendekat dan dia sendiri kemudian mengakui bahwa kami telah terlalu jauh berjalan. Sadar saya membutuhkan pertolongan secepatnya, dia menggendong 
saya di atas punggungnya lalu berlari sembari membujuk-bujuk saya untuk tetap tenang. Napasnya memburu kelelahan, tapi rasa tanggung jawab yang besar 
seperti memberinya kekuatan berlipat. Sayang, sesampai di rumah bukan lain 
yang didapatnya kecuali caci maki Ayah dan Ibu. Pipinya sempat pula kena tampar Ayah yang murka.
Saya langsung dilarikan ke puskesmas kecamatan. Seragam coklat Pramuka yang melingkupi tubuh saya disingkirkan entah ke mana oleh mantri. Tidak pernah 
terlintas di pikiran saya untuk meminta kepada Ayah agar menggantinya setelah itu. Dari yang saya dengar selama hampir sebulan tidak masuk sekolah, beberapa 
kali dia terpaksa membolos di hari Jumat dan Sabtu karena belum mampu membeli gantinya. ”Salahmu sendiri, tidak minta ganti,” kata saya selesai kami mengingat kejadian 
itu. ”Mengajakmu saja sudah sebuah kesalahan. Aku takut ayahmu bertambah marah nantinya. Ayahku tidak mau mempermasalahkan tamparan ayahmu, apalagi 
seragam itu. Dia lebih memilih membelikan yang baru walaupun harus menunggu beberapa minggu.”
Kami tertawa. Tertawa dan tertawa seakan-akan seluruh rentetan kejadian yang akhirnya menjadi pengingat abadi persahabatan kami itu bukanlah sebuah 
kejadian meloloskan diri dari maut karena waktu telah menghapus semua kengeriannya.
Dia lalu mengajak saya ke halaman belakang di mana kami pernah bersama-sama membuat kolam gurami. Kolam itu sudah tiada, diuruk sejak lama berganti 
menjadi sebuah gudang tempatnya kini berkreasi membuat kerajinan dari bambu. Hasil dari tangan terampilnya itu ditambah pembagian keuntungan sawah garapan
milik orang lainlah yang menghidupi istri dan dua anaknya hingga kini Ayah dan ibunya sudah meninggal, tapi sebuah masalah berat kini menjeratnya. 
Dia bercerita, sertifikat rumah dan tanah peninggalan orangtua justru tergadaikan.
”Kakakku itu, masih sama sifatnya seperti kau mengenalnya dulu. Hanya kini, semakin tua dia semakin tidak tahu diri.”
”Ulahnya?” Dia mengangguk. ”Kau tahu, rumah dan tanah yang tidak seberapa luas ini adalah milik kami paling 
berharga. Tapi aku tidak kuasa untuk menolak kemauannya mencari pinjaman modal usaha dengan mengagunkan semuanya. Aku percaya padanya, peduli 
padanya. Tapi, dia tidak memiliki rasa yang sama terhadapku. Dia mengkhianati kepercayaanku. Usahanya kandas dan kini beban berat ada di pundakku.” 
Terbayang sosok kakaknya dahulu, seorang remaja putus sekolah yang selalu menyusahkan orangtua dengan kenakalan-kenakalannya. Kini setelah beranjak 
tua, masih pula dia menyusahkan adik satu-satunya. ”Kami akan bertahan,” katanya tersenyum saat melepas saya setelah hari beranjak
sore. Ada kesungguhan dalam suaranya.Sepanjang perjalanan pulang, pikiran saya tidak pernah lepas dari sahabat saya 
yang baik itu. Saya malu. Sebagai sahabat, saya merasa belum pernah berbuat baik padanya. Tidak pula yakin akan mampu melakukan seperti yang 
dilakukannya untuk menolong saya di malam itu. </Text>
                </View>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Dia telah membuktikan bahwa keberanian dan rasa tanggung jawab yang besar bisa timbul dari sebuah 
persahabatan yang tulus.Mata saya kemudian melirik seragam dinas yang tersampir di sandaran jok 
belakang. Sebagai jaksa yang baru saja menangani satu kasus perdata, seragam itu belum bisa membuat saya bangga. Nilainya jelas jauh lebih kecil dibanding nilai 
persahabatan yang saya dapatkan dari sebuah seragam coklat Pramuka. Tapi dia tidak tahu, dengan seragam dinas itu, sayalah yang akan mengeksekusi 
pengosongan tanah dan rumahnya</Text>
                    <Text style={{fontSize: 25, marginLeft: 10,}}>SANG JUARA</Text>
                    <Text style={{fontSize: 17, marginLeft: 10,}}>Karya Zuha Farhanani</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Bu guru masuk kekelasku. Aku sedang mencoret-coret kertas yang tak terpakai di
dalam laci mejaku. Pelajaran hari ini adalah Matematika. Yaps ! salah satu mata
pelajaran yang tidak aku sukai.
“Anak-anak sekarang kita akan membahas tentang rumus phytagoras atau tripel
phytagoras. Buka halaman 54.” Kata umi berjilbab hijau. Atau biasa dipanggil
umi Dini. “Umi,ada urusan sebentar. Kalian tunggu dan kerjakan latihan dulu ya.” Perintahnya
“Ya bu.” Kata murid 8.a serempak Ah,males ngerjainnya. Ngerti juga nggak. Nyontek aja deh. Gumamku
“Rida kamu nggak ngerjain,ta ?” tanya Eka “Males. Aku nggak ngerti.” Jawabku “Ih,kamu ini jangan males lah. Ntar kamu turun nilainya.” Katanya sambil
meninggalkanku Bodo penting aku nggak bodoh di pelajaaran ini. Bu Dini masuk kembali kekelas. “Anak-anak gimana sudah belum latihannya ? kalau belum selesaikan di rumah.
Waktu kita tak cukup kalau hanya digunakan untuk latihan saja.” Terang beliau Kami pun menyimpan buku dan dikerjakan di rumah. Umi Dini menerangkan
tentang phytagoras phytagoras sedangkan aku hanya melamun saja sampai pelajaran selesai...</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Sepulang sekolah aku diajak umi ke toko buku. Tak sengaja aku melihat sebuah
buku novel terbitan DAR! Mizan aku tertarik pada buku itu. Q pinta umi untuk membelikannya. Umi pun menurutiku beliau membelikannya.
Aku tak sabar ingin membaca buku itu. Aku masuk ke dalam kamar dan membacanya.
Oleh : Mia Al-marfa'i Dalam hati aku berkata pintar sekali orang itu dapat membuat novel ini kataku. Aku ingin sekali menjadi seperti dia. Namun,bagiku itu tak mungkin karena
prestasiku sangatlah rendah. Namun,aku akan berusaha agar menjadi sang juara.</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Mulai sekarang dan seterusnya aku harus belajar lebih giat lagi. Ku kerjakan
pekerjaan rumah dengan tekun. Dan kucoba untuk menyukai guru-gurunya. Dan kukurangi smsan dengan teman-teman. Alhamdulillah nilaiku bertambah tinggi.
Aku sangat senang dan aku bersyukur pada Allah. Ulangan semester II pun telah didepan mata. Aku optimis insyaallah aku bisa
mengerjakannya. Matpelnya hari ini Bahasa Indonesia dan Agama. Aku baca so'al dengan teliti an
aku isi lembar jawaban menggunakan pensil. Selama seminggu ulangan semester telah selesai. Dan menunggu hasilnya.</Text>
                </View>
                <View style={{height:640,width:350, backgroundColor:'white' , borderColor:'silver',borderRadius: 10,marginTop:8, marginLeft:7, borderBottomWidth:2}}>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Hari ini class meeting,banyak lomba yang diadakan. Aku salah satu panita lomba
tari daerah. “Hey,kira-kira siapa yang akan menduduki juara 1 lagi ya ?” kata Dinda
“Ehm,kukira tetap Nanda deh. So'alnya dia itu pintar banget. Jagonya Fisika.”
Ujar Fenti “Yah,mereka emang pintar coba aku jadi dia mungkin aku bisa membanggakan ortuku.” Kata Mia sambil memakan ice cream.
“Ga,pasti Tian. So'alnya dia jago matematika dan biologi.” Ujar Asep “Ah,kalo aku mah dukung Oki. Dia pintar dan juga sopan santunnya bagus.” Kata
Wina nggak mau ngalah. So'alnya Oki adalah kejarannya dari kelas 7,wajar dukung Oki terus.
Aku diam saja. Aku berdo'a semoga aku bisa mengalahkan 3 pesaingku itu. Aku memang dulu mempunayi prestasi sewaktu kelas 7. namun,ntah tiba-tiba hilang
kemana dia. (kayak benda aja bisa ilang. Heheh)</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Waktu pembagian rapor telah tiba. Aku tak sabar ingin melihat hasilku yang
selama ini aku lakukan dengan ikhlas. Semoga memuaskan “pkirku”.
Pak Handri memasuki lapangan dan membuka upacara kenaikan bendera. Hari itu tepat bertepatan dengan hari senin. Jadi sekalian pembagian rapor juga menaikan
sang merah putih. “Baiklah akan saya sebutkan juara-juara kelas. Saya mulai dari kelas 7.” Kata pak Handri 
</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Semua murid terlihat tegang termasuk aku. Adik kelasku telah disebutkan siapa
juara kelasnya. Sekarang giliran kelas 8.1 jantungku deg-degkan saat mendengarnya. “Juara 3 diraih oleh : Septian Praja Pradana dengan jumlah nilan 932. juara 2 di
raih oleh : Malik riski anugerah. Juara pertama diraih oleh.......... Jantungku bertambah kuat memompa dan saat mendengar kata
Adalah ...... Faridha Aisyah Inabi... sorak sorai tepuk tangan berkumpul ditelingaku. Aku tak menyangka aku dapat rangking 1. terima kasih Ya Allah...
Teman-temanku mengucapkan selamat padaku. Termasuk Oki yang sang juara juga mengucapkan selamat padaku.
“Terimakasih teman-teman.” Kataku </Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}>Kulangkahkan kaki ku dengan riang. Aku tak sabar ingin memberitahukan ini
kepada Umi dan Abi. Aku pun langsung masuk kedalam rumah dan menyalami orangtuaku, “Umi,abi Rida punya kejutan.” Kataku
“Apa itu,sayang ?” tanya Umi “Umi,alhamdulillah Rida dapet ranking satu.” Kataku
Umi dan abi mengucapkan selamat dan memelukku. Aku senang sekali. Aku akan terus giat belajar. Walaupun begitu aku tak boleh sombong. Aku harus belajar
walaupun aku menjadi juara 1.
</Text>
                    <Text style={{fontSize: 15, marginLeft: 10,}}></Text>
                </View>
            </ScrollView>
        </View>
    )
}
