import React,{Component}from 'react';
import { Text, View, StyleSheet,ScrollView, TouchableOpacity, ImageBackground, Image } from 'react-native';


export default class Menu extends Component {
    render (){
        return(
            <View>
            <ScrollView vertical>
            <View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 10}}>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 5, marginLeft:5}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Sahabat')}>  
                        <Image source={require('../assets/shbt.jpg')} style={{height:110, width:110, marginLeft:20, marginTop:9}}/>
                        </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:20, marginTop: 5}}>Sahabat Sejati</Text>
                    </View>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 0, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Cerpen1')}>
                    <Image source={require('../assets/maaf.jpg')} style={{height:110, width:110, marginLeft:20, marginTop:9}}/>
                    </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:26, marginTop: 5}}> Maafkan Aku Tuhan</Text>
                    </View>
            </View>


            <View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 20}}>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 10, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Cerpen2')}> 
                    <Image source={require('../assets/baru.jpg')} style={{height:100, width:110, marginLeft:20, marginTop:9}}/>
                        </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:20, marginTop: 5}}>USIA 17 DAN PACAR BARU</Text>
                    </View>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 0, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Cerpen3')}> 
                    <Image source={require('../assets/love.png')} style={{height:100, width:110, marginLeft:20, marginTop:9}}/>
                    </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:26, marginTop: 5}}>Love Story</Text>
                    </View>
            </View>


            <View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 20}}>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 10, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Cerpen4')}>
                    <Image source={require('../assets/sebuah.jpg')} style={{height:100, width:110, marginLeft:20, marginTop:9}}/>
                    </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:20, marginTop: 5}}>Sebuah Persahabatan</Text>
                    </View>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 0, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Tentang')}>
                    <Image source={require('../assets/arti.jpg')} style={{height:110, width:110, marginLeft:20, marginTop:9}}/>
                    </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:26, marginTop: 5}}>Arti Sebuah Sahabat</Text>
                    </View>
            </View>

            <View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 20}}>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 10, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Seragam')}>
                    <Image source={require('../assets/seragam.jpg')} style={{height:100, width:110, marginLeft:20, marginTop:9}}/>
                    </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:20, marginTop: 5}}>Seragam</Text>
                    </View>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 0, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Segumpal')}>
                    <Image source={require('../assets/satu.jpg')} style={{height:110, width:110, marginLeft:20, marginTop:9}}/>
                    </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:26, marginTop: 5}}>Segumpal Sesal</Text>
                    </View>
            </View>



            <View style={{flexDirection:'row', justifyContent:'space-around', marginTop: 20}}>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 10, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Ibu')}>
                    <Image source={require('../assets/ibu.jpg')} style={{height:100, width:110, marginLeft:20, marginTop:9}}/>
                    </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:20, marginTop: 5}}>Ibu</Text>
                    </View>
                    <View style={{height:150, width:150, backgroundColor:'white', borderRadius: 0, marginLeft:10}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Cerpen5')}>
                    <Image source={require('../assets/atm.jpg')} style={{height:110, width:110, marginLeft:20, marginTop:9}}/>
                    </TouchableOpacity>
                        <Text style={{fontSize:10, fontWeight:'bold', marginLeft:26, marginTop: 5}}>Kartu ATM Ku</Text>
                    </View>
            </View>
            </ScrollView>
        </View>
       
    );

    };
        
}
