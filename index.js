import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Splash from '../pages/Splash';
import {Menu, PartSatu, Segumpal, Seragam} from '../pages';
import Tabnavigation from '../tabsnavigation/Tabnavigation';
import Sahabat from '../pages/Sahabat';
import Setting from '../pages/Setting';
import Cerpen1 from '../pages/Cerpen1';
import Cerpen2 from '../pages/Cerpen2';
import Cerpen3 from '../pages/Cerpen3';
import Cerpen4 from '../pages/Cerpen4';
import Cerpen5 from '../pages/Cerpen5';
import seragam from '../pages/Seragam';
import segumpal from '../pages/Segumpal';
import Ibu from '../pages/Ibu';
import Tentang from'../pages/Tentang';
import Inputnama from '../pages/Inputnama';



const Navigasi = createNativeStackNavigator();

const Route = () => {
  return (
    <Navigasi.Navigator>
      <Navigasi.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
      <Navigasi.Screen name="Home" component={Tabnavigation} options={{headerShown:false}}/>
      <Navigasi.Screen name="PartSatu" component={PartSatu} />
      <Navigasi.Screen name="Menu" component={Menu} />
      <Navigasi.Screen name="Sahabat" component={Sahabat}/>
      <Navigasi.Screen name="Cerpen1" component={Cerpen1}/>
      <Navigasi.Screen name="Cerpen2" component={Cerpen2}/>
      <Navigasi.Screen name="Cerpen3" component={Cerpen3}/>
      <Navigasi.Screen name="Cerpen4" component={Cerpen4}/>
      <Navigasi.Screen name="Cerpen5" component={Cerpen5}/>
      <Navigasi.Screen name="Setting" component={Setting}/>
      <Navigasi.Screen name="Seragam" component={Seragam}/>
      <Navigasi.Screen name="Segumpal" component={Segumpal}/>
      <Navigasi.Screen name="Ibu" component={Ibu}/>
      <Navigasi.Screen name="Tentang" component={Tentang}/>
      <Navigasi.Screen name="Inputnama" component={Inputnama}/>
    </Navigasi.Navigator>
  );
};

export default Route;
